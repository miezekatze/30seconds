package com.mieze.thirtysec;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameRule;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.CommandAPIConfig;
import dev.jorel.commandapi.arguments.Argument;
import dev.jorel.commandapi.arguments.IntegerArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import dev.jorel.commandapi.executors.PlayerCommandExecutor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;

public class Thirtysec extends JavaPlugin implements Listener {
    BukkitRunnable runnable;
    int max_time = (60 * 2) * 20;
    int time = 0;
    BossBar bar;
    Scoreboard board;
    Team team;
    Objective objective;

    private static class PlayerData {
        public Location bedrock;
        public int max_z = 0;
        public String name = "<unknown>";
        public int penalty = 0;

        public PlayerData(String name, int x) {
            this.bedrock = new Location(Bukkit.getWorld("gameworld"), x, 60, 0);
            this.name = name;
        }
    }

    private static HashMap<UUID, PlayerData> playerData = new HashMap<>();

    @Override
    public void onLoad() {
        CommandAPI.onLoad(new CommandAPIConfig().silentLogs(false));
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        var uuid = event.getPlayer().getUniqueId();
        if (playerData.containsKey(uuid)) {
            event.setRespawnLocation(playerData.get(uuid).bedrock.clone().add(0, 1, 0));
        }
    }

    private final static int PENALTY = 3;

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        var uuid = event.getEntity().getUniqueId();
        if (playerData.containsKey(uuid)) {
            playerData.get(uuid).penalty += PENALTY;
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        var player = event.getPlayer();
        var uuid = player.getUniqueId();
        var pos = player.getLocation();
        if (! playerData.containsKey(uuid)) {
            return;
        }
        var bedrock = playerData.get(uuid).bedrock;

        if (pos.getX() - bedrock.getX() > 5) {
            var p = pos.clone();
            p.setX(bedrock.getX() + 5);
            player.teleport(p, TeleportCause.PLUGIN);
        }
        if (pos.getX() - bedrock.getX() < -5) {
            var p = pos.clone();
            p.setX(bedrock.getX() - 5);
            player.teleport(p, TeleportCause.PLUGIN);
        }

        if (pos.getZ() < 0) return;
        if ((int)pos.getZ() <= playerData.get(uuid).max_z) return;
        // check connectivity
        var world = Bukkit.getWorld("gameworld");
        var block = bedrock.clone().add(0, 0, 0);
        
        if (! checkBlocks(Position.fromBlock(block), world, Position.fromBlock(pos), new HashSet<>())) {
            return;
        }
        playerData.get(uuid).max_z = (int)pos.getZ();
    }

    private final static int[] dv = new int[]{0, -1, 1};

    private record Position(int x, int y, int z) {
        public Position add(int dx, int dy, int dz) {
            return new Position(x + dx, y + dy, z + dz);
        }

        public static Position fromBlock(Location loc) {
            new HashSet<Integer>().add(1);
            return new Position(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        }
    }

    private boolean checkBlocks(Position start, World world, Position end, Set<Position> visited) {
        for (int dz = 1; dz >= 0; dz--) {
            for (int dx : dv) {
                for (int dy : dv) {
                    if (dx == 0 && dy == 0 && dz == 0) continue;
                    var block = start.add(dx, dy, dz);
                    if (visited.contains(block)) continue;
                    visited.add(block);
                    if (block.equals(end)) return true;
                    if (world.getBlockAt(block.x(), block.y(), block.z()).getType() == Material.AIR) continue;
                    if (checkBlocks(block, world, end, visited)) return true;
                }
            }
        }
        return false;
    }

    private void spawnRandomItems() {
        var world = Bukkit.getWorld("gameworld");
        world.getPlayers().forEach(player -> {
            var uuid = player.getUniqueId();
            var data = playerData.get(uuid);
            
            if (data.penalty > 0) {
                player.sendMessage("§cYou have a penalty of " + data.penalty + " rounds!");
                data.penalty--;
                return;
            }

            Material material;
            do {
                material = Material.values()[(int) (Math.random() * Material.values().length)];
            } while (!material.isItem());

            var bedrock = data.bedrock.clone();
            var loc = bedrock.add(0, 1, 0);
            var entity = world.dropItem(loc, new ItemStack(material, 1));
            entity.setVelocity(new Vector(0, 0, 0));
        });
    }

    private String formatTime(int secs) {
        return String.format("%02d:%02d:%02d", secs / (60 * 60), secs / 60 % 60, secs % 60);
    }
    private void start() {
        time = max_time;
        int num = 0;
        var world = new WorldCreator("gameworld")
            .copy(Bukkit.getWorld("world"))
            .type(WorldType.FLAT)
            .generateStructures(false)
            .generatorSettings("{\"lakes\":false,\"features\":false,\"layers\":[{\"block\":\"minecraft:air\",\"height\":1}],\"structures\":{\"structures\":{}}}")
            .createWorld();
        world.setAutoSave(false);
        world.setDifficulty(Difficulty.EASY);
        world.setGameRule(GameRule.KEEP_INVENTORY, true);

        if (bar != null) {
            bar.removeAll();
            bar = null;
        }

        if (board != null) {
            board.clearSlot(DisplaySlot.SIDEBAR);
            board = null;
        }

        board = Bukkit.getScoreboardManager().getMainScoreboard();
        board.clearSlot(DisplaySlot.SIDEBAR);
        board.getTeams().forEach(Team::unregister);
        board.getObjectives().forEach(Objective::unregister);

        team = board.registerNewTeam("30s");
        team.setPrefix(ChatColor.RED + "[Playing] " + ChatColor.RESET);
        team.setDisplayName("30s");
        team.setAllowFriendlyFire(false);

        objective = board.registerNewObjective("30s", "dummy", "30s");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§6§l30 Seconds");

        bar = Bukkit.createBossBar("Next drop: ?s", org.bukkit.boss.BarColor.GREEN, org.bukkit.boss.BarStyle.SOLID);
        for (var p : Bukkit.getOnlinePlayers()) {
            var uuid = p.getUniqueId();
            playerData.put(uuid, new PlayerData(p.getName(), num));

            var bedrock = playerData.get(uuid).bedrock;
            var block = bedrock.clone();
            world.getBlockAt(block).setType(org.bukkit.Material.BEDROCK);
            world.getBlockAt(block.add(1, 1, 0)).setType(org.bukkit.Material.BEDROCK);
            world.getBlockAt(block.add(0, 1, 0)).setType(org.bukkit.Material.BEDROCK);
            world.getBlockAt(block.add(-2, 0, 0)).setType(org.bukkit.Material.BEDROCK);
            world.getBlockAt(block.add(0, -1, 0)).setType(org.bukkit.Material.BEDROCK);
            world.getBlockAt(block.add(1, 0, -1)).setType(org.bukkit.Material.BEDROCK);
            world.getBlockAt(block.add(0, 1, 0)).setType(org.bukkit.Material.BEDROCK);
            p.teleport(bedrock.add(0.5, 1, 0.5), TeleportCause.PLUGIN);

            p.getInventory().clear();
            bar.addPlayer(p);
            num += 10;
        }

        runnable = new BukkitRunnable() {
            @Override
            public void run() {
                if (time == 0) {
                    Bukkit.broadcastMessage("time's up!");
                    cancel();
                    return;
                }

                int secs = time / 20;

                // update action bar
                Bukkit.getOnlinePlayers().forEach(p -> {
                    var timeStr = formatTime(secs);
                    p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(timeStr));
                    if (bar != null && ! bar.getPlayers().contains(p)) {
                        bar.addPlayer(p);
                    }
                });

                // update boss bar
                int ticksLeft = time % (30*20);
                bar.setProgress(1.0 - ((double) ticksLeft / (30.0*20)));
                bar.setTitle("Next drop: " + (ticksLeft / 20) + "s");

                if (time % 20 == 0) {
    
                    // update scoreboard
                    // remove all
                    for (var p : board.getTeam("30s").getEntries()) {
                        board.resetScores(p);
                    }
                    for (var o : objective.getScoreboard().getEntries()) {
                        objective.getScoreboard().resetScores(o);
                    }

                    int val = 0;
                    int place = Math.min(playerData.size(), 10);
                    for (var p : playerData.values().stream().sorted((a, b) -> a.max_z - b.max_z).limit(10).toList()) {
                        var team = board.getTeam("30s");
                        if (!team.hasEntry(p.name))
                            team.addEntry(p.name);
                        objective.getScore(ChatColor.YELLOW + "" + place + ". " + ChatColor.GREEN + p.name + ": " + ChatColor.RESET + p.max_z + " blocks").setScore(val);
                        val++;
                        place--;
                    }

                    objective.getScore(" ").setScore(val++);
                    objective.getScore("§eTime left: §f" + formatTime(secs) + "s").setScore(val++);
                    objective.getScore("  ").setScore(val++);
                }

                if (ticksLeft == 0) {
                    spawnRandomItems();
                }

                time--;
            }
        };
        runnable.runTaskTimer(this, 0, 1);
    }

    private void reset() {
        playerData.clear();
        runnable.cancel();
        bar.removeAll();
        bar = null;

        // reset team
        for (var t : team.getEntries()) {
            team.removeEntry(t);
        }
        for (var o : objective.getScoreboard().getEntries()) {
            objective.getScoreboard().resetScores(o);
        }

        board.clearSlot(DisplaySlot.SIDEBAR);
        board.getTeams().forEach(Team::unregister);
        board.getObjectives().forEach(Objective::unregister);
        board = null;

        Bukkit.getOnlinePlayers().forEach(p -> {
            p.getInventory().clear();
            p.teleport(new Location(Bukkit.getWorld("world"), 8, 1, 8), TeleportCause.PLUGIN);
        });

        Arrays.stream(Bukkit.getWorld("gameworld")
            .getLoadedChunks())
            .forEach(c -> c.unload(false));

        Bukkit.unloadWorld("gameworld", false);
        try {
            FileUtils.deleteDirectory(new File(Bukkit.getWorldContainer(), "gameworld"));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private PlayerCommandExecutor handleCommand(String command) {
        return switch (command) {
            case "start" -> (player, args) -> {
                if (!player.isOp()) {
                    player.sendMessage("you don't have permission to start the game!");
                    return;
                }

                if (args == null || args.length == 0 || (int)args[0] <= 0) {
                    player.sendMessage("Please provice a time in minutes!");
                    return;
                }
                max_time = (int) args[0] * 20 * 60;

                start();
            };
            case "reset" -> (player, _y) -> {
                if (!player.isOp()) {
                    player.sendMessage("you don't have permission to reset the game!");
                    return;
                }

                reset();
            };
            default -> (_x, _y) -> Bukkit.broadcastMessage("invalid command `/challenge " + command + "'! (THIS IS A PLUGIN BUG)");
        };
    }

    private void addCommandVariant(String s, Argument<?>... args) {
        Stream<Argument<?>> a = Stream
            .of(Stream.of(new LiteralArgument(s)), Stream.of(args))
            .flatMap(x -> x);

        new CommandAPICommand("30s")
            .withArguments(a.toList())
            .executesPlayer(handleCommand(s))
            .register();
    }

    private void registerCommand() {
        addCommandVariant("start", new IntegerArgument("minutes"));
        addCommandVariant("reset");
    }
    
    @Override
    public void onEnable() {
        CommandAPI.onEnable(this);
        Bukkit.getPluginManager().registerEvents(this, this);
        Bukkit.getLogger().info("Hello, World!");

        registerCommand();
    }

    @Override
    public void onDisable() {
        CommandAPI.onDisable();
        Bukkit.getLogger().info("Goodbye, World!");
    }
}
